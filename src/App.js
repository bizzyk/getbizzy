// App.js
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import {Sidebar,Footer,ProfileProvider, ProtectedRoute} from './components';
import {About,Contact,CreateIssue, Home, Logout, Entries, Settings, Login} from './pages';
import React, { useState } from "react";
import { AuthProvider } from './AuthContext';
import './components/Axios';
import './App.css';

function App() {
    const [isSidebarVisible, setIsSidebarVisible] = useState(true);

    const toggleSidebar = () => {
        setIsSidebarVisible(!isSidebarVisible);
    };

    return (
        <Router>
            <AuthProvider>
                <ProfileProvider>
                    {/* Apply flex properties to the App container */}
                    <div className="App">
                        {/* Sidebar should remain on one side */}
                        <Sidebar toggleSidebar={toggleSidebar} isSidebarVisible={isSidebarVisible} />

                        {/* Main content section */}
                        <div className={isSidebarVisible ? "content" : "content-full"}>
                            <Routes>
                                <Route path="/" element={<Home />} />
                                <Route path="/about" element={<About />} />
                                <Route path="/contact" element={<Contact />} />
                                <Route path="/login" element={<Login />} />

                                {/* Protected routes */}
                                <Route path="/create-issue" element={<ProtectedRoute><CreateIssue /></ProtectedRoute>} />
                                <Route path="/entries" element={<ProtectedRoute><Entries /></ProtectedRoute>} />
                                <Route path="/settings" element={<ProtectedRoute><Settings /></ProtectedRoute>} />
                                <Route path="/logout" element={<ProtectedRoute><Logout /></ProtectedRoute>} />
                            </Routes>
                        </div>
                    </div>
                    <Footer />
                </ProfileProvider>
            </AuthProvider>
        </Router>
    );
}

export default App;