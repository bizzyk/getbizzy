// In src/pages/Main.js

import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Main from "../components/Main";


const Home = () => {
    return (
        <div>
            <Main />
        </div>
    );
};

export default Home;