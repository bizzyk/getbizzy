import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import { supabase } from '../supabaseClient';
import Chatbot from '../components/Chatbot';
import axios from 'axios';

const CreateIssue = () => {
    const [repos, setRepos] = useState([]);
    const [labels, setLabels] = useState([]);
    const [issueTypes, setIssueTypes] = useState([]);
    const [repo, setRepo] = useState(null);
    const [selectedLabels, setSelectedLabels] = useState([]);
    const [description, setDescription] = useState('');
    const [issueType, setIssueType] = useState(null);
    const [userId, setUserId] = useState(null);
    const [previewContent, setPreviewContent] = useState('');
    const [chatbotResponse, setChatbotResponse] = useState('');
    const [title, setTitle] = useState('');

    const handleRepoChange = (selectedRepo) => setRepo(selectedRepo);
    const handleLabelsChange = (selectedLabels) => setSelectedLabels(selectedLabels);
    const handleIssueTypeChange = (selectedType) => {
        setIssueType(selectedType);
        if (selectedType) {
            const prompt = `Reference the markdown template for ${selectedType.label}`;
            console.log('Using prompt:', prompt);
            setChatbotResponse(`Generated response for ${selectedType.label}`);
        }
    };

    useEffect(() => {
        async function fetchData() {
            const { data: { session } } = await supabase.auth.getSession();
            if (session && session.user) {
                setUserId(session.user.id);
                const { data: reposData } = await supabase
                    .from('user_repositories')
                    .select('*')
                    .eq('user_id', session.user.id);
                setRepos(reposData?.map(repo => ({
                    value: repo.id,
                    label: repo.repo_name,
                    repo_url: repo.repo_url,
                    access_token: repo.access_token
                })) || []);

                const { data: labelsData } = await supabase
                    .from('labels')
                    .select('*')
                    .eq('user_id', session.user.id);
                setLabels(labelsData?.map(label => ({ value: label.id, label: label.label_name })) || []);

                const { data: issueTypesData } = await supabase
                    .from('issues_entry')
                    .select('*')
                    .eq('creator', session.user.id);
                setIssueTypes(issueTypesData.map(type => ({ value: type.id, label: type.title })));
            }
        }
        fetchData().catch(console.error);
    }, []);

    useEffect(() => {
        console.log('chatbotResponse state changed:', chatbotResponse);
    }, [chatbotResponse]);

    const handleSubmitToGitLab = async () => {
        if (!repo || !title || !chatbotResponse) {
            alert('Please fill out all required fields.');
            return;
        }

        const selectedRepo = repos.find(r => r.value === repo.value);
        if (!selectedRepo) {
            alert('Selected repository not found.');
            return;
        }

        const { repo_url: repoUrl, access_token: accessToken } = selectedRepo;

        const payload = {
            title: title,
            description: chatbotResponse,
            labels: selectedLabels.map(label => label.label),
            repo_url: repoUrl,
            access_token: accessToken
        };

        console.log('Sending payload to server:', payload);

        try {
            const response = await axios.post('http://localhost:8000/api/create-issue/', payload); // Make sure this URL matches your Django server
            if (response.status === 201) {
                alert('Issue created successfully!');
            } else {
                alert('Failed to create issue. Please try again.');
            }
        } catch (error) {
            console.error('Error creating issue:', error);
            alert('An error occurred. Please try again.');
        }
    };

    return (
        <div className="contact-container mt-5 create-issue" style={{ width: '75%', margin: '0 auto' }}>
            <h1 className="text-center">Create Issue</h1>

            {/*<div className="form-group">*/}
            {/*    <Select*/}
            {/*        id="issue-type-select"*/}
            {/*        value={issueType}*/}
            {/*        onChange={handleIssueTypeChange}*/}
            {/*        options={issueTypes}*/}
            {/*        placeholder="Select Issue Type"*/}
            {/*        isClearable*/}
            {/*    />*/}
            {/*</div>*/}

            <div className="form-group" style={{ height: 'auto' }}>
                <Chatbot onChatbotResponse={setChatbotResponse} />
            </div>

            <div className="form-group">
                <textarea
                    id="chatbot-response-title"
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                    rows="1"
                    className="form-control"
                    placeholder="Enter issue title here..."
                />
            </div>

            <div className="form-group">
                <textarea
                    id="chatbot-response"
                    value={chatbotResponse}
                    onChange={(e) => setChatbotResponse(e.target.value)}
                    rows="10"
                    className="form-control"
                    placeholder="Paste the issue content here..."
                />
            </div>

            <div className="form-group">
                <Select
                    id="repo-select"
                    value={repo}
                    onChange={handleRepoChange}
                    options={repos}
                    placeholder="Select Repository"
                    isClearable
                />
            </div>

            <div className="form-group">
                <Select
                    id="labels-select"
                    value={selectedLabels}
                    onChange={handleLabelsChange}
                    options={labels}
                    placeholder="Select Labels"
                    isMulti
                />
            </div>

            <div className="preview-container" style={{ marginTop: '20px' }}>
                {previewContent && (
                    <div>
                        <h3>Issue Preview:</h3>
                        <textarea className="form-control" value={previewContent} readOnly rows="10"></textarea>
                    </div>
                )}
            </div>

            <div className="form-group">
                <button onClick={handleSubmitToGitLab} className="btn btn-secondary">
                    Submit to GitLab
                </button>
            </div>
        </div>
    );
};

export default CreateIssue;
