import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../AuthContext';  // Adjust the path as necessary

const Logout = () => {
    const { logout } = useAuth();
    const navigate = useNavigate();

    useEffect(() => {
        // Ensure logout completes before navigating
        const performLogout = async () => {
            await logout();  // Assume logout is an async function
            navigate('/');  // Redirect to home page after logging out
        };

        performLogout().then(r => console.log('Logged out successfully'));  // Log success message
    }, [logout, navigate]);  // Dependencies for useEffect

    // Optionally display a message while logging out
    return (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
            <h1>Logging out...</h1>
        </div>
    );
};

export default Logout;
