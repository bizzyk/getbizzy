import React, { useEffect, useState } from 'react';
import { toast, ToastContainer } from 'react-toastify';
import { useForm } from 'react-hook-form';
import 'react-toastify/dist/ReactToastify.css';
import { supabase } from '../supabaseClient';

const Entries = () => {
    const [entries, setEntries] = useState([]);
    const [selectedEntry, setSelectedEntry] = useState(null); // To track which entry is being edited
    const { register, handleSubmit, reset, setValue } = useForm();


    // Fetch all entries from the public.issues_entry table
    const fetchEntries = async () => {
        const { data: { session } } = await supabase.auth.getSession();
        if (!session || !session.user) {
            toast.error('User session not found');
            return;
        }


        const { data, error } = await supabase
            .from('issues_entry')
            .select('*')
            .eq('creator', session.user.id);

        if (error) {
            console.error('Error fetching entries:', error.message);
            toast.error('Error fetching entries.');
        } else {
            setEntries(data || []);
        }
    };

    const editEntry = (entry) => {
        setSelectedEntry(entry); // Track the entry being edited
        // Set form field values directly in the editEntry function
        setValue('title', entry.title, { shouldValidate: true });
        setValue('content', entry.content, { shouldValidate: true });
    };

    const saveEntry = async (data) => {
        const { data: { session } } = await supabase.auth.getSession();
        if (!session || !session.user) {
            toast.error('User session not found');
            return;
        }

        const entry = {
            creator: session.user.id,
            title: data.title,
            content: data.content,
        };

        let response;
        try {
            if (selectedEntry) {
                response = await supabase
                    .from('issues_entry')
                    .update(entry)
                    .eq('id', selectedEntry.id);
            } else {
                response = await supabase
                    .from('issues_entry')
                    .insert([entry]);
            }

            if (response.error) {
                throw new Error(response.error.message);
            }

            toast.success('Entry saved successfully!');
            reset();
            setSelectedEntry(null);
            fetchEntries();
        } catch (error) {
            console.error('Error saving entry:', error.message);
            toast.error('Error saving entry.');
        }
    };


    const deleteEntry = async (id) => {
        if (window.confirm('Are you sure you want to delete this entry?')) {
            const { error } = await supabase
                .from('issues_entry')
                .delete()
                .eq('id', id);

            if (error) {
                console.error('Error deleting entry:', error.message);
                toast.error('Error deleting entry.');
            } else {
                toast.success('Entry deleted successfully!');
                fetchEntries();
                setSelectedEntry(null);
            }
        }
    };

    useEffect(() => {
        fetchEntries();
    }, []);

    useEffect(() => {
        if (selectedEntry) {
            setValue('title', selectedEntry.title, { shouldValidate: true });
            setValue('content', selectedEntry.content, { shouldValidate: true });
        } else {
            reset();
        }
    }, [selectedEntry, setValue, reset]);

    return (
        <div className="contact-container mt-5" style={{width: '75%', margin: '0 auto'}}>
            <h1 className="text-center">Templates</h1>
            <form onSubmit={handleSubmit(saveEntry)}
                  style={{maxHeight: selectedEntry ? '500px' : 'unset', overflowY: 'auto'}}>
                <div className="form-group">
                    <input {...register('title', {required: true})} placeholder="Enter title" className="form-control"/>
                </div>
                <div className="form-group">
                    <textarea {...register('content', {required: true})} placeholder="Enter content"
                              className="form-control" rows={10}/>
                </div>
                <div className="text-center">
                    <button type="submit" className="btn btn-secondary">
                        {selectedEntry ? 'Update' : 'Add'}
                    </button>
                </div>
            </form>


            <ToastContainer/>
            <div className="form-group">
                <div className="repo-details">
                    <ul className="list-unstyled">
                        {entries.map((entry) => (
                            <li key={entry.id}
                                className="list-group-item d-flex justify-content-between align-items-center rounded shadow-sm p-3 mb-2 bg-white">
                                <span>{entry.title}</span>
                                <div>
                                    <button className="btn btn-edit" onClick={() => editEntry(entry)}>Edit
                                    </button>
                                    <button className="btn btn-delete" onClick={() => deleteEntry(entry.id)}>Delete
                                    </button>
                                </div>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>

        </div>
    );
};

export default Entries;