// In src/pages/Login.js

import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../AuthContext';
import { supabase } from '../supabaseClient';


const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [message, setMessage] = useState('');
    const [isNewUser, setIsNewUser] = useState(false); // To toggle between Sign In and Sign Up
    const navigate = useNavigate();
    const { user } = useAuth();

    useEffect(() => {
        // If user is already logged in, inform them and redirect to profile page
        if (user) {
            alert('You are already logged in.');
            navigate('/settings');
        }
    }, [user, navigate]);

    const handleLogin = async (event) => {
        event.preventDefault();
        if (!email || !password) {
            setError('Please enter both email and password.');
            return;
        }
        const method = isNewUser ? 'signUp' : 'signInWithPassword';
        const { user, error } = await supabase.auth[method]({
            email,
            password
        });

        if (error) {
            console.error('Error during login:', error);
            setError(error.message);
        } else if (user) {
            if (isNewUser) {
                setMessage('Please check your email for verification to finish creating your account.');
                setEmail('');
                setPassword('');
                setIsNewUser(false); // Optionally reset the sign-up form or redirect
            } else {
                navigate('/');
            }
        }
    };

    const handleOAuthLogin = async (provider) => {
        const { user, error } = await supabase.auth.signInWithOAuth({ provider });
        if (error) {
            console.error('OAuth login error:', error);
            setError(error.message);
        } else if (user) {
            navigate('/settings');
        }
    };

    return (
        <div className="login-container mt-5">
            <h1 className="text-center">{user ? 'Log Out' : isNewUser ? 'Create Account' : 'Log In'}</h1>
            <form onSubmit={handleLogin} className="mb-3">
                <div className="form-group">
                    <input
                        type="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Email"
                        className="form-control"
                    />
                </div>
                <div className="form-group">
                    <input
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder="Password"
                        className="form-control"
                    />
                </div>
                <button type="submit" className="btn btn-primary w-100">{isNewUser ? 'Sign Up' : 'Log In'}</button>
                {error && <p className="text-danger">{error}</p>}
                {message && <p className="text-success">{message}</p>} {/* Display success message */}
            </form>
            <button onClick={() => handleOAuthLogin('github')} className="btn btn-secondary w-100 mb-2">Sign In with GitHub</button>
            <button onClick={() => handleOAuthLogin('gitlab')} className="btn btn-secondary w-100 mb-2">Sign In with GitLab</button>
            <button onClick={() => handleOAuthLogin('google')} className="btn btn-secondary w-100 mb-2">Sign In with Google</button>
            <button onClick={() => setIsNewUser(!isNewUser)} className="btn btn-link w-100 mt-3">
                {isNewUser ? 'Already have an account? Log In' : 'Need an account? Sign Up (You will need to confirm your email address before signing in for the first time.)'}
            </button>
        </div>
    );
};

export default Login;
