// In src/pages/Contact.js

import React, { useState } from 'react';
import {supabase} from "../supabaseClient";

const Contact = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');
    const [submitted, setSubmitted] = useState(false);

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            const { data, error } = await supabase.from('contacts').insert([
                { name, email, message }
            ]);
            if (error) {
                console.error('Error inserting data:', error.message);
                // Handle error, e.g., display error message to the user
            } else {
                console.log('Data inserted successfully:', data);
                // Handle successful submission, e.g., display confirmation message to the user
                setSubmitted(true);
            }
        } catch (error) {
            console.error('Error inserting data:', error.message);
            // Handle error, e.g., display error message to the user
        }
    };


    return (
        <div className="contact-container mt-5 create-issue" style={{width: '75%', margin: '0 auto'}}>
            <h1 className="text-center">Contact Us</h1>
            {submitted ? (
                <div className="text-center">
                    <p>Thank you for your message! We'll get back to you soon.</p>
                </div>
            ) : (
                <form onSubmit={handleSubmit}>
                    <div className="form-group">
                        <input
                            type="text"
                            value={name}
                            onChange={(e) => setName(e.target.value)}
                            placeholder="Your Name"
                            className="form-control"
                            required
                        />
                    </div>
                    <div className="form-group">
                        <input
                            type="email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            placeholder="Your Email"
                            className="form-control"
                            required
                        />
                    </div>
                    <div className="form-group">
                        <textarea
                            value={message}
                            onChange={(e) => setMessage(e.target.value)}
                            placeholder="Your Message"
                            className="form-control"
                            required
                        ></textarea>
                    </div>
                    <div style={{display: 'flex', justifyContent: 'center'}}>
                        <button type="submit" className="btn btn-secondary">Submit</button>
                    </div>
                </form>
            )}
            <div className="slack-link mt-3 text-center">
                <h2>Join our Slack workspace for more discussions and updates:</h2>
                <a href="https://join.slack.com/t/getbizzy-workspace/shared_invite/zt-2gu03nrit-_TlW37gKnf7ZabJw27kXjQ"
                   target="_blank" rel="noopener noreferrer" className="btn btn-secondary">Join Slack</a>
            </div>
        </div>
    );
};

export default Contact;