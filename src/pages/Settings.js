// In src/pages/Settings.js
import React from 'react';
import UserProfile from '../components/UserProfile';
import RepositorySettings from '../components/RepositorySettings';
import IssueLabelSettings from '../components/IssueLabelSettings';
import ErrorBoundary from '../components/ErrorBoundary';


const Settings = () => {
    return (
        <div className="contact-container mt-5" style={{width: '95%', margin: '0 auto'}}>
            <h1 className="text-center">Settings</h1>
            <UserProfile/>
            <RepositorySettings/>
            <ErrorBoundary>
                <IssueLabelSettings/>
            </ErrorBoundary>
        </div>
    );
};

export default Settings;