// In pages/index.js
export { default as Home } from './Home';
export { default as About } from './About';
export { default as Contact } from './Contact';
export { default as CreateIssue } from './CreateIssue';
export { default as Logout } from './Logout';
export { default as Login } from './Login';
export { default as Settings } from './Settings';
export { default as Entries } from './Entries';