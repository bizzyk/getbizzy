// In src/pages/About.js

import React from 'react';

const About = () => {
    return (
        <div className="contact-container mt-5 create-issue" style={{width: '75%', margin: '0 auto'}}>
            <h1 className="text-center">AI-Driven Product Management</h1>
            <h2 style={{textAlign: 'center', fontSize: '18px', marginTop: '20px'}}>
                getBizzy.pro is your dynamic and revolutionary product management companion.
                Harness the power of our AI technology to transform your operations,
                generate issues blazingly fast using customizable templates, and
                choose your GitLab instance from a comfy dropdown menu.
            </h2>
            <div style={{display: 'flex', justifyContent: 'space-around', marginTop: '50px'}}>
                <div style={{margin: '10px'}}>
                    <h2>Issue Generation</h2>
                    <h3>Immerse yourself in technology and let AI churn out markdown formatted issues.</h3>
                    <h2>Automation via AI</h2>
                    <h3>Let us do the heavy-lifting! Automate your user story creation & sprint reports.</h3>
                </div>
                <div style={{margin: '10px'}}>
                    <h2>Easy Project Selection</h2>
                    <h3>Effortlessly select the right GitLab project using our user-friendly dropdown menu.</h3>
                    <h2>Sprint Report Compilation</h2>
                    <h3>Join dots easily with automatic issue and story compilation into sprint reports.</h3>
                </div>
                <div style={{margin: '10px'}}>
                    <h2>Google Account Integration</h2>
                    <h3>Log in securely and conveniently using your Google account.</h3>
                    <h2>Release Note Creation</h2>
                    <h3>Say goodbye to hassle! Systematically bundle issues and stories into inclusive release notes.</h3>
                </div>
            </div>
        </div>
    );
};

export default About;
