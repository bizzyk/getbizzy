import React from 'react';
import { supabase } from '../supabaseClient';

function Auth() {
    const handleLogin = async (provider) => {
        const { error } = await supabase.auth.signInWithOAuth({ provider });
        if (error) console.error('Error logging in:', error.message);
    };

    return (
        <div>
            <button onClick={() => handleLogin('google')}>Log in with Google</button>
            <button onClick={() => handleLogin('github')}>Log in with GitHub</button>
            <button onClick={() => handleLogin('gitlab')}>Log in with GitLab</button>
        </div>
    );
}

export default Auth;
