// ProtectedRoute.js
import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import { useAuth } from '../AuthContext';

const ProtectedRoute = ({ children }) => {
    const { isAuthenticated } = useAuth();
    const location = useLocation();

    // Log location for debugging
    console.log('Attempting to access:', location.pathname);

    if (!isAuthenticated) {
        return <Navigate to="/" state={{ from: location }} />;
    }

    // If authenticated, allow access to the requested route
    return children;
};

export default ProtectedRoute;
