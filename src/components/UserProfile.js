// src/components/UserProfile.js
import React, { useState, useEffect } from 'react';
import { supabase } from '../supabaseClient';

const UserProfile = () => {
    const [username, setUsername] = useState('');
    const [profilePic] = useState(null);
    const [previewUrl, setPreviewUrl] = useState(null);

    // Fetch user data from Supabase using session data
    useEffect(() => {
        const fetchUserProfile = async () => {
            // Retrieve session data and user info
            const { data: { session } } = await supabase.auth.getSession();
            if (session && session.user) {
                const userId = session.user.id;
                console.log('Authenticated user:', session.user);

                // Fetch the profile data with `maybeSingle` for safety
                const { data: profileData, error } = await supabase
                    .from('profiles')
                    .select('*')
                    .eq('id', userId)
                    .maybeSingle();
                if (error) {
                    console.error('Error fetching profile:', error.message);
                } else if (profileData) {
                    setUsername(profileData.preferred_name);
                    if (profileData.avatar_url) {
                        setPreviewUrl(profileData.avatar_url);
                    }
                } else {
                    console.warn('No profile found for the current user.');
                }
            } else {
                console.error('No authenticated user found.');
            }
        };

        fetchUserProfile();
    }, []);

    // Upload the avatar file and return the URL
    const uploadAvatar = async (file, userId) => {
        if (!userId) {
            console.error('Missing user ID for avatar upload');
            return null;
        }

        const fileName = `public/avatars/${userId}/${file.name}`;

        // Upload file to storage bucket
        const { error } = await supabase.storage
            .from('avatars')
            .upload(fileName, file, { upsert: true });

        if (error) {
            console.error('Error uploading avatar:', error.message);
            return null;
        }

        // Retrieve the public URL for the uploaded file
        const { publicURL, error: urlError } = supabase.storage
            .from('avatars')
            .getPublicUrl(fileName);

        if (urlError) {
            console.error('Error getting public URL:', urlError.message);
            return null;
        }

        return publicURL;
    };

    // Use this to retrieve the authenticated session and user information
    const getAuthenticatedUser = async () => {
        const { data: { session }, error } = await supabase.auth.getSession();
        if (error || !session || !session.user) {
            console.error('Unable to retrieve session:', error);
            return null;
        }
        return session.user;
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        // Retrieve the authenticated user
        const user = await getAuthenticatedUser();
        if (!user) {
            console.error('Unable to update profile: No authenticated user.');
            return;
        }

        // Proceed with the profile update logic
        let avatarUrl = previewUrl;
        if (profilePic) {
            avatarUrl = await uploadAvatar(profilePic, user.id);
        }

        const { error } = await supabase
            .from('profiles')
            .upsert({
                id: user.id,
                preferred_name: username,
                avatar_url: avatarUrl,
            });

        if (error) {
            console.error('Profile update failed:', error.message);
        }
    };

    // // Handle the file selection event
    // const handleFileChange = (event) => {
    //     const file = event.target.files[0];
    //     if (file) {
    //         setProfilePic(file);
    //         const previewUrl = URL.createObjectURL(file);
    //         setPreviewUrl(previewUrl);
    //     }
    // };

    return (
        <div className="profile-settings">
            <form onSubmit={handleSubmit}>
                <div className="header-container">
                    <h1 style={{margin: '0'}}>User Profile</h1>
                    <button type="submit" className="btn btn-secondary">Save Changes</button>
                </div>
                <div className="form-row">
                    <div className="form-group">
                        <label>

                            <input
                                type="text"
                                value={username}
                                className="form-control"
                                onChange={(e) => setUsername(e.target.value)}
                                placeholder="Your Preferred Name"
                            />
                        </label>
                    </div>
                    {/*<div className="form-group">*/}
                    {/*    <label>*/}
                    {/*        Profile Picture:*/}
                    {/*        <input*/}
                    {/*            type="file"*/}
                    {/*            className="form-control"*/}
                    {/*            accept="image/*"*/}
                    {/*            onChange={handleFileChange}*/}
                    {/*        />*/}
                    {/*    </label>*/}
                    {/*</div>*/}
                </div>
                {/*{previewUrl && <img src={previewUrl} alt="Profile Preview" width="100"/>}*/}
            </form>
        </div>
    )
        ;
};

export default UserProfile;
