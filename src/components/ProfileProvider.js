// ProfileProvider.js
import React, { createContext, useContext, useState, useEffect } from 'react';
import { supabase } from '../supabaseClient';
import { useAuth } from '../AuthContext';

const ProfileContext = createContext();


export const useProfile = () => useContext(ProfileContext);

export const ProfileProvider = ({ children }) => {
    const { user, isAuthenticated } = useAuth();
    const [profile, setProfile] = useState({ preferred_name: null, avatar_url: null });

    const fetchProfileData = async (userId) => {
        const { data, error } = await supabase
            .from('profiles')
            .select('preferred_name, avatar_url')
            .eq('id', userId)
            .single();

        if (error) {
            console.error('Error fetching profile data:', error.message);
        } else {
            setProfile(data);
        }
    };

    useEffect(() => {
        if (isAuthenticated && user) {
            fetchProfileData(user.id);
        }
    }, [isAuthenticated, user]);

    return (
        <ProfileContext.Provider value={{ profile }}>
            {children}
        </ProfileContext.Provider>
    );
};


export default ProfileProvider;