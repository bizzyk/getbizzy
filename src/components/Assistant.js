
// In src/pages/CreateIssue.js
import React, { useState, useEffect } from 'react';
import Select from 'react-select';
import { supabase } from '../supabaseClient';
import { toast } from 'react-toastify';
import {useForm} from "react-hook-form";
import OpenAI from "openai";


async function createAssistant() {
    const apiKey = process.env.REACT_APP_OPENAI_API_KEY;
    const model = process.env.REACT_APP_OPENAI_API_MODEL;
    const assistant = process.env.REACT_APP_OPENAI_API_ASSISTANT;
    const vs = process.env.REACT_APP_OPENAI_VS_ID;
    const instructions = process.env.REACT_APP_OPENAI_INSTRUCTIONS;


     const requestBody = {
        model: model,
        instructions: instructions,
        temperature: 1,
        max_tokens: 2048,
        top_p: 1,
        prompt: prompt,
        tools: [{type: "file_search"}],
        tool_resources: {"file_search": {
                "vector_store_ids": [vs]
            }},
        stop: ["\n"],
    };

    try {
        const response = await fetch("https://api.openai.com/v1/assistants", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${apiKey}`,
                'OpenAI-Beta': 'assistants=v2'
            },
            body: JSON.stringify(requestBody)
        });

        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }

        const data = await response.json();
        console.log('Assistant Created:', data);
        return data;
    } catch (error) {
        console.error('Failed to create assistant:', error);
    }
}