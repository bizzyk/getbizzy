// AuthHelpers.js

import { supabase } from '../supabaseClient';


// Refreshes the access token using the existing session management functions
export const refreshAccessToken = async () => {
    const { data: { session }, error } = await supabase.auth.getSession();
    if (error || !session) {
        console.error('No active session or error retrieving session:', error);
        return null;
    }

    console.log('Session before refresh:', session);

    const { data, error: refreshError } = await supabase.auth.setSession(session);
    if (refreshError) {
        console.error('Error refreshing session:', refreshError.message);
        return null;
    }

    console.log('Access token refreshed:', data.session.access_token);
    return data.session.access_token;
};


export default refreshAccessToken;
