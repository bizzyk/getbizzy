import React from 'react';

const Footer = () => {
    return (
        <footer className="footer">
            <span>© 2024 getBizzy.pro - All rights reserved</span>
        </footer>
    );
};

export default Footer;
