// src/components/RepositorySettings.js

import React, {useState, useEffect} from 'react';
import {supabase} from '../supabaseClient';
import CryptoJS from 'crypto-js'; // Import the library

const RepositorySettings = () => {
    // State variables for repositories and form inputs
    const [repositories, setRepositories] = useState([]);
    const [newRepoName, setNewRepoName] = useState('');
    const [newRepoUrl, setNewRepoUrl] = useState('');
    const [username, setUsername] = useState('');
    const [accessToken, setAccessToken] = useState('');
    const [showAddForm, setShowAddForm] = useState(false);
    const [userId, setUserId] = useState(null);

    // Fetch user's repositories using session data
    useEffect(() => {
        const fetchRepositories = async () => {
            const {data: {session}} = await supabase.auth.getSession();
            if (session && session.user) {
                setUserId(session.user.id);
                console.log('Authenticated user:', session.user);

                // Fetch repositories associated with this user
                const {data: repositoriesData, error} = await supabase
                    .from('user_repositories')
                    .select('*')
                    .eq('user_id', session.user.id);
                if (error) {
                    console.error('Error fetching repositories:', error.message);
                } else {
                    setRepositories(repositoriesData);
                }
            } else {
                console.error('No authenticated user found.');
            }
        };

        fetchRepositories();
    }, []);

    // Handle new repository submission
    const handleAddRepository = async (event) => {
        event.preventDefault();

        // Encrypt the access token before sending it to the database
        const encryptedToken = CryptoJS.AES.encrypt(accessToken, process.env.REACT_APP_ENCRYPTION_KEY).toString();

        // Ensure the user ID is available before inserting
        const {data, error} = await supabase
            .from('user_repositories')
            .insert([{
                user_id: userId,
                repo_name: newRepoName,
                repo_url: newRepoUrl,
                username,
                access_token: encryptedToken
            }]);
        if (error) {
            console.error('Error adding repository:', error.message);
        } else {
            // Assuming the response data is an object containing the newly added repository
            setRepositories(repositories => [...repositories, ...data]);
            setNewRepoName('');
            setNewRepoUrl('');
            setUsername('');
            setAccessToken('');
        }
    };


    const handleDeleteRepository = async (repoId) => {
        // A confirmation prompt before proceeding with deletion
        const isConfirmed = window.confirm('Are you sure you want to delete this repository?');
        if (!isConfirmed) {
            return; // Stop deletion if not confirmed
        }

        const {error} = await supabase
            .from('user_repositories')
            .delete()
            .eq('id', repoId);

        if (error) {
            console.error('Error deleting repository:', error.message);
        } else {
            // Update local state to remove the deleted repository
            setRepositories(repositories.filter(repo => repo.id !== repoId));
        }
    };

    return (
        <div className="repository-settings">
            <div className="header-container">
                <h1 style={{margin: '0'}}>Repository Settings</h1>
                <button onClick={() => setShowAddForm(!showAddForm)} className="btn btn-secondary">
                    Add New
                </button>
            </div>
            {showAddForm && (
                <form onSubmit={handleAddRepository}
                      style={{display: "flex", alignItems: "center", gap: "10px"}}>
                    <div style={{display: "flex", gap: "10px"}}>

                        <input
                            type="text"
                            value={newRepoName}
                            placeholder={"Repository Name"}
                            className="form-control"
                            onChange={(e) => setNewRepoName(e.target.value)}
                            style={{ flex: 1 }}
                        />

                        <input
                            type="text"
                            value={newRepoUrl}
                            placeholder={"Repository URL"}
                            className="form-control"
                            onChange={(e) => setNewRepoUrl(e.target.value)}
                            style={{ flex: 1 }}
                        />
                    </div>
                    <div style={{display: "flex", gap: "10px", alignItems: "center"}}>

                        <input
                            type="text"
                            value={username}
                            placeholder={"Username"}
                            className="form-control"
                            onChange={(e) => setUsername(e.target.value)}
                            style={{ flex: 1 }}
                        />

                        <input
                            type="password"
                            className="form-control"
                            placeholder={"Access Token"}
                            value={accessToken}
                            onChange={(e) => setAccessToken(e.target.value)}
                            style={{ flex: 1 }}
                        />

                        <button type="submit" className="btn btn-secondary">Add Repository</button>
                    </div>
                </form>
            )}

            <ul>
                {repositories && repositories.length > 0 ? (
                    repositories.map((repo) => {
                        if (!repo) {
                            return null; // Skip this iteration if the repo is null or undefined
                        }
                        // Safe to assume repo is not null from this point
                        const urlPart = repo.repo_url.replace(/^https?:\/\//, '');

                        return (
                            <li key={repo.id} className="repository-item">
                                <div className="repo-details-actions">
                                    <div className="repo-details">
                                        {/* Replace the access token part with asterisks for security */}
                                        {`https://${repo.username}:*******@${urlPart}`}
                                    </div>
                                    <div className="repo-actions">
                                        <button onClick={() => handleDeleteRepository(repo.id)} className="btn btn-delete">
                                            Delete
                                        </button>
                                    </div>
                                </div>
                            </li>
                        );
                    })
                ) : (
                    <p>Loading repositories or no repositories found...</p> // Display a loading message or "not found" message as appropriate
                )}

            </ul>
        </div>
    );
}

export default RepositorySettings;
