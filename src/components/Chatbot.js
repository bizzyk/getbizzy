import React, { useEffect } from 'react';

export default function Chatbot({ onChatbotResponse }) {
    const customStyle = {
        width: '100%',
        height: '350px', // Adjust this value as needed
        border: '2px solid #e2e8f0',
        borderRadius: '0.375rem',
        boxShadow: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
    };

    useEffect(() => {
        function handleMessage(event) {
            console.log('Message received from iframe:', event.data);
            console.log('Message origin:', event.origin);

            if (event.data && event.data.type === 'chatbot_response') {
                console.log('Chatbot response received:', event.data.message);
                onChatbotResponse(event.data.message);
                const iframe = document.getElementById('openassistantgpt-chatbot-iframe');
                if (iframe) {
                    iframe.style.display = 'none';
                }
            } else if (event.data && event.data.type === 'openChat') {
                const iframe = document.getElementById('openassistantgpt-chatbot-iframe');
                if (iframe) {
                    iframe.style.display = 'block';
                    iframe.style.pointerEvents = 'auto';
                }
            } else {
                console.log('Other message received:', event.data);
            }
        }

        window.addEventListener('message', handleMessage);

        return () => {
            window.removeEventListener('message', handleMessage);
        };
    }, [onChatbotResponse]);

    return (
        <div>
            <iframe
                src="https://www.openassistantgpt.io/embed/clwdjw66o0001lludcnykoskw/window?chatbox=false&withExitX=true"
                style={customStyle}
                title={'OpenAssistant GPT Chatbot'}
                id="openassistantgpt-chatbot-iframe"
                onLoad={() => {
                    const iframe = document.getElementById('openassistantgpt-chatbot-iframe');
                    console.log('Iframe loaded, sending addListener message');
                    iframe.contentWindow.postMessage({ type: 'addListener' }, '*');
                }}
            ></iframe>
        </div>
    );
}
