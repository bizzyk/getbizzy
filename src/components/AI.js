//ai.js

import OpenAI from "openai";
import { toast } from 'react-toastify';
import {useState} from "react";

const apiKey = process.env.REACT_APP_OPENAI_API_KEY;
const model = process.env.REACT_APP_OPENAI_API_MODEL;
const vectorStoreID = process.env.REACT_APP_OPENAI_VS_ID;

// Initialize OpenAI directly with the apiKey
const openai = new OpenAI({
    apiKey: apiKey,
    dangerouslyAllowBrowser: true
});


const Bizzy = () => {
    const [previewContent, setPreviewContent] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            const response = await openai.createCompletion({
                model: model,
                prompt: "You will be provided with an issue type needed. The issue type will match one of the template files. You provide the requested story or epic or other issue type matching the template in markdown. You always respond in markdown format using the template format.",
                temperature: 1,
                max_tokens: 2048,
                top_p: 1,
                tools: [{ type: "file_search" }],
                tool_resources: {
                    "file_search": {
                        "vector_store_ids": [vectorStoreID]
                    }
                }
            });

            console.log('Generated Issue:', response.data.choices[0].text);
            toast.info('Preview ready!');
            setPreviewContent(response.data.choices[0].text);
        } catch (error) {
            console.error('Error generating issue:', error);
            toast.error('Failed to generate preview.');
        }
    };


    return (
        <div>
            <button onClick={handleSubmit}>Generate Issue</button>
            <div>{previewContent}</div>
        </div>
    );
};

export default Bizzy;