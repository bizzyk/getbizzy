// Sidebar.js
import React, {useState} from 'react';
import {NavLink} from 'react-router-dom';
import {useAuth} from '../AuthContext';
import 'bootstrap/dist/css/bootstrap.min.css'; // Import Bootstrap CSS
import {FaAngleLeft, FaAngleRight, FaUser} from 'react-icons/fa';
import {CgTemplate} from "react-icons/cg";
import {MdLogin, MdLogout, MdOutlineChat, MdSettings} from 'react-icons/md';
import {HiHome} from 'react-icons/hi';
import {IoCreate, IoInformationCircleOutline} from 'react-icons/io5';

const Sidebar = ({toggleSidebar, isSidebarVisible}) => {
    const {isAuthenticated, user} = useAuth();
    const [showSettings, setShowSettings] = useState(false);

    const toggleSettings = (e) => {
        e.stopPropagation(); // Prevent parent click events from interfering
        setShowSettings(!showSettings);
    };

    return (
        <div className={`sidebar ${isSidebarVisible ? 'visible' : 'hidden'}`}>
            <div className="sidebar-logo">
                <a href="/" className="sidebar-logo">
                    <img src="/logoWhite.png" alt="Logo"/>
                </a>
            </div>
            <button className="toggle-sidebar" onClick={toggleSidebar} aria-label="Toggle Sidebar">
                {isSidebarVisible ? <FaAngleLeft size={15}/> : <FaAngleRight size={15}/>}
            </button>
            <ul className="sidebar-top">
                <li>
                    <NavLink to="/" className={({isActive}) => (isActive ? "active" : undefined)}>
                        <HiHome size={20}/> Home
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/about" className={({isActive}) => (isActive ? "active" : undefined)}>
                        <IoInformationCircleOutline size={20}/> About
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/contact" className={({isActive}) => (isActive ? "active" : undefined)}>
                        <MdOutlineChat size={20}/> Contact
                    </NavLink>
                </li>
                {isAuthenticated && (
                    <>
                        <li>
                            <NavLink to="/create-issue" className={({isActive}) => (isActive ? "active" : undefined)}>
                                <IoCreate size={20}/> Create Issue
                            </NavLink>
                        </li>
                        <li>
                            <NavLink to="/entries" className={({isActive}) => (isActive ? "active" : undefined)}>
                                <CgTemplate size={20}/> Templates
                            </NavLink>
                        </li>
                    </>
                )}
            </ul>
            {isAuthenticated && (
                <div className="sidebar-bottom" onClick={toggleSettings}>
                    <button
                        onClick={(e) => {
                            e.preventDefault();
                            toggleSettings(e);
                        }}
                        className="sidebar-user-info"
                    >
                        <FaUser size={20} style={{verticalAlign: 'middle'}}/>
                        <span title={user ? user.email : 'User'}>
                        {user ? ` ${user.preferred_name || user.email}` : ' User'}
                        </span>
                    </button>

                    {showSettings && (
                        <ul className="sidebar-settings">
                            <li>
                                <NavLink to="/settings" className={({isActive}) => (isActive ? "active" : undefined)}>
                                    <MdSettings size={20}/> Settings
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/logout" className={({isActive}) => (isActive ? "active" : undefined)}>
                                    <MdLogout size={20}/> Logout
                                </NavLink>
                            </li>
                        </ul>
                    )}
                </div>
            )}
            {!isAuthenticated && (
                <ul className="sidebar-bottom">
                    <li>
                        <NavLink to="/login" className={({isActive}) => (isActive ? "active" : undefined)}>
                            <MdLogin/> Login
                        </NavLink>
                    </li>
                </ul>
            )}
        </div>
    );
}

export default Sidebar;
