// src/components/IssueLabelSettings.js
import React, { useState, useEffect } from 'react';
import { supabase } from '../supabaseClient';
import { IoCloseSharp } from "react-icons/io5";

const IssueLabelSettings = () => {
    // State variables for labels and form inputs
    const [labels, setLabels] = useState([]);
    const [newLabelName, setNewLabelName] = useState('');
    const [newLabelColor, setNewLabelColor] = useState('');
    const [showAddForm, setShowAddForm] = useState(false);
    const [userId, setUserId] = useState(null);
    const [editingLabelId, setEditingLabelId] = useState(null); // State to track the label being edited



    // Fetch user's labels
    useEffect(() => {
        const fetchLabels = async () => {
            const { data: { session } } = await supabase.auth.getSession();
            if (session && session.user) {
                setUserId(session.user.id); // Set the userId to be used later
                console.log('Authenticated user:', session.user);

                // Fetch labels from the database for this user
                const { data, error } = await supabase
                    .from('labels')
                    .select('*')
                    .eq('user_id', session.user.id);
                if (error) {
                    console.error('Error fetching labels:', error.message);
                } else {
                    setLabels(data); // Update state with the fetched labels
                }
            } else {
                console.error('No authenticated user found.');
            }
        };

        fetchLabels();
    }, []);

    // Handle new label submission or editing existing label
    const handleAddOrEditLabel = async (event) => {
        event.preventDefault();

        if (userId) { // Now checks the state-based userId
            // Check for duplicate labels
            const duplicate = labels.some(label => label.label_name === newLabelName && label.id !== editingLabelId);
            if (duplicate) {
                alert('A label with this name already exists.');
                return;
            }

            if (editingLabelId) {
                // Update existing label
                const { error } = await supabase
                    .from('labels')
                    .update({
                        label_name: newLabelName,
                        label_color: newLabelColor
                    })
                    .eq('id', editingLabelId)
                    .eq('user_id', userId);

                if (error) {
                    console.error('Error updating label:', error.message);
                } else {
                    setLabels(currentLabels => currentLabels.map(label =>
                        label.id === editingLabelId ? { ...label, label_name: newLabelName, label_color: newLabelColor } : label
                    ));
                    setEditingLabelId(null);
                }
            } else {
                // Insert new label
                const { data, error } = await supabase
                    .from('labels')
                    .insert([{
                        user_id: userId,
                        label_name: newLabelName,
                        label_color: newLabelColor
                    }]);

                if (error) {
                    console.error('Error adding label:', error.message);
                } else {
                    setLabels(currentLabels => [...currentLabels, ...(Array.isArray(data) ? data : [data])]);
                }
            }

            setNewLabelName('');
            setNewLabelColor('');
            setShowAddForm(false);
        } else {
            console.error('Unable to add or edit label: No authenticated user.');
        }
    };

    const handleDeleteLabel = async (labelId) => {
        const isConfirmed = window.confirm('Are you sure you want to delete this label?');
        if (!isConfirmed) {
            return; // Stop the deletion if the user does not confirm
        }
        const { error } = await supabase
            .from('labels')
            .delete()
            .eq('id', labelId);

        if (error) {
            console.error('Error deleting label:', error.message);
        } else {
            // Filter out the deleted label from the local state to update the UI
            setLabels(labels.filter(label => label.id !== labelId));
        }
    };

    const handleEditLabel = (label) => {
        setNewLabelName(label.label_name);
        setNewLabelColor(label.label_color);
        setEditingLabelId(label.id);
        setShowAddForm(true);
    };

    return (
        <div className="label-settings">
            <div className="header-container">
                <h1 style={{margin: '0'}}>Label Settings</h1>
                <button onClick={() => { setShowAddForm(!showAddForm); setEditingLabelId(null); setNewLabelName(''); setNewLabelColor(''); }} className="btn btn-secondary">
                    {showAddForm ? 'Cancel' : 'Add New'}
                </button>
            </div>
            {showAddForm && (
                <form onSubmit={handleAddOrEditLabel}
                      style={{display: "flex", alignItems: "center", gap: "10px"}}>

                    <input
                        type="text"
                        value={newLabelName}
                        placeholder="New Label Name"
                        className="form-control"
                        onChange={(e) => setNewLabelName(e.target.value)}
                        style={{width: "auto", flex: "1"}} // Custom width or flex to adjust size as needed
                    />

                    <input
                        type="text"
                        placeholder="Label Color"
                        value={newLabelColor}
                        className="form-control"
                        onChange={(e) => setNewLabelColor(e.target.value)}
                        style={{width: "auto", flex: "1"}} // Custom width or flex to adjust size as needed
                    />

                    <button type="submit" className="btn btn-secondary">
                        {editingLabelId ? 'Update Label' : 'Add Label'}
                    </button>
                </form>
            )}

            <div className="label-tiles">
                {labels.map((label) => (
                    <div key={label?.id} className="label-tile"
                         style={{
                             backgroundColor: label?.label_color ?? '#f0f0f0',
                             position: 'relative',
                             display: 'flex',
                             alignItems: 'center',  // Align items vertically
                             justifyContent: 'space-between', // Space between label text and close icon
                             padding: '5px', // Add some padding around content
                         }}>
                        <span onClick={() => handleEditLabel(label)} style={{cursor: 'pointer'}}>{label?.label_name}</span>
                        <IoCloseSharp
                            onClick={() => handleDeleteLabel(label.id)}
                            style={{color: 'red', position: 'absolute', top: 0, right: 0, cursor: 'pointer', fontSize: '12px'}} // Make the icon a bit larger and clickable
                        />
                    </div>
                ))}
            </div>

        </div>
    );
};

export default IssueLabelSettings;