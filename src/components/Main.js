// In src/pages/Main.js

import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';


const Main = () => {
    return (
        <div style={{ height: '100vh', width: '75%', margin: '0 auto'}}>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '50vh' }}>
                <img src="/logoWhite.png" alt="Logo" style={{
                    width: "40vw"
                }} />
            </div>

            <h2 style={{
                fontFamily: '"Unica One", sans-serif',
                fontSize: "38px",
                textAlign: "center",
                color: "white",
                padding: "60px",
            }}>
                Life’s too short for tangled product management. Let’s getBizzy!
            </h2>

            <div className="container text-center">
                <Link to="/login" className="btn btn-secondary m-2" aria-label="Sign up today">
                    Sign Up Today
                </Link>
            </div>
        </div>
    );
};

export default Main;