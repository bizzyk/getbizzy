// AuthContext.js in the src directory

import React, { createContext, useContext, useState, useEffect } from 'react';
import { supabase } from './supabaseClient';

const AuthContext = createContext();

export const useAuth = () => useContext(AuthContext);

export const AuthProvider = ({ children }) => {
    const [user, setUser] = useState(null);
    const [isAuthenticated, setIsAuthenticated] = useState(false);

    useEffect(() => {
        const { data: authListener } = supabase.auth.onAuthStateChange((event, session) => {
            setUser(session?.user || null);
            setIsAuthenticated(!!session?.user);
        });

        // This logs the structure to ensure we know what's inside authListener
        console.log("Auth Listener:", authListener);

        return () => {
            if (authListener && typeof authListener.subscription.unsubscribe === 'function') {
                authListener.subscription.unsubscribe();
            } else {
                console.error('Expected unsubscribe function not found:', authListener);
            }
        };
    }, []);



    const login = async (email, password) => {
        const { user, error } = await supabase.auth.signInWithPassword({ email, password });  // Use signIn for logging in
        if (error) {
            console.error('Login error:', error.message);
            return false;
        }
        setUser(user);
        setIsAuthenticated(true);
        return true;
    };

    const logout = async () => {
        const { error } = await supabase.auth.signOut();
        if (error) {
            console.error('Logout error:', error.message);
            return false;
        }
        setUser(null);
        setIsAuthenticated(false);
        return true;
    };

    return (
        <AuthContext.Provider value={{ isAuthenticated, user, login, logout }}>
            {children}
        </AuthContext.Provider>
    );
};

