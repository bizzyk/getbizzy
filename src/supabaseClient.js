// src/supabaseClient.js
import { createClient } from '@supabase/supabase-js'

const supabaseUrl = process.env.REACT_APP_SUPABASE_URL;
const supabaseKey = process.env.REACT_APP_SUPABASE_CLIENT_API_KEY;

if (!supabaseUrl || !supabaseKey) {
    console.error('Supabase URL and Key must be set.');
}

export const supabase = createClient(supabaseUrl, supabaseKey);