## :rocket: Release Notes

:guardian-one: **APP:** USSF Guardian One 

:calendar: **WHEN:** <!-- Prod Release Date -->

:package: **WHAT:** <!-- Release Version -->

:sparkles:  **What's New:**

 - **New Feature:**  <!-- if any add -->
 - **Content Update:**  <!-- if any add -->
 - **Interface Improvement:**  <!-- if any add -->
 - **Tool Enhancement:** <!-- if any add -->
 - **Functional Fix:** <!-- if any add -->

:mega: **MARKETING ANNOUNCE:** <!-- Yes or No -->

- [ ] Cantina Mobile App [Release Ticket](https://repo.bespin.cce.af.mil/bespin/service-desk/service-desk-issues/-/issues/new) Submitted [Link ticket]

/label ~release