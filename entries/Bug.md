## :bug: Description
<!-- Please provide a short description of the bug. -->

#### What I expected to happen
<!-- Please provide a step by step summary of what the expected behavior is. -->
1. 

#### What actually happened
<!-- Please provide a step by step summary of what was actually observed. -->
1. 

#### What _SHOULD_ happen
<!-- Please provide a step by step summary of how the team expects the system to respond. -->
<!-- This is to be filled out during Backlog Grooming -->
1. 

## :camera_with_flash: Screenshots or screen recordings:


#### Platform
- [ ] iOS
- [ ] Android
- [ ] Web

#### OS version and Browser version (if applicable)
<!-- Example below -->


#### App version (build)
<!-- Example below -->


#### Affected environment(s)
- [ ] Development
- [ ] Staging
- [ ] Production

#### Severity of the issue
- [ ] High severity
- [ ] Medium severity
- [ ] Low severity

#### Visibility
- [ ] User visible
- [ ] Stakeholder visible

/label ~bug