# New Mobile App Release

Please fill in the following information for your new mobile app release:

## Landing Zones

* [x] Apple App Store (Public)
* [ ] Apple App Store (Apple Business Manager / B2B)
* [ ] Apple App Store (TestFlight Internal)
* [x] Apple App Store (TestFlight External)
* [x] Google Play Store (Public)
* [ ] Google Play Store (Managed / B2B)
* [x] Google Play Store (Internal Beta)
* [x] Google Play Store (Closed Beta)
* [ ] Google Play Store (Open Beta)
* [ ] MDM
  * [ ] ACC (IBM MaaS360)
  * [ ] ACC (Ivanti MobileIron)
  * [ ] AFRC (VMWare WS1 UEM)
* [ ] Side-load (Apple Configurator + USB / Android Side Load)

## Version

Version: 1.1.11

Build: 357

## Release Date
App Stores:
* [x] Release immediately after Apple / Google app review
* [ ] Release on specific date / time
  * (Date / Time)
* [ ] App Team will release to MDM directly via Cantina

## :rocket: Release Notes

<div style="text-align: center;">
  <img src="https://repo.bespin.cce.af.mil/groups/bespin/products/USSF/Licensed/-/wikis/uploads/e092c1e562ef1b795bfc2d97a8ecee68/G1blackLogo.png" width="200">
</div>

**APP:** USSF Guardian One 

:calendar: **WHEN:** 4/8/24

:package: **WHAT:** 1.1.11 (357)

:sparkles:  **What's New:**

 - **Interface Improvement:**  New splash page and icon

:mega: **MARKETING ANNOUNCE:** No

## Pipeline

Link to `develop` pipeline for demo/test app: https://repo.bespin.cce.af.mil/bespin/products/USSF/Licensed/guardian-one/-/pipelines

Link to `main` pipeline for prod app: https://repo.bespin.cce.af.mil/bespin/products/USSF/Licensed/guardian-one/-/pipelines

/cc @chris.forant_clarity @Bryant_Fearless @max.gipson_clarity @swright_fearless @steven.song_bespin @marc.brown_bespin @tmikesell_clarity @sarah_clarity
