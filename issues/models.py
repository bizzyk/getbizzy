# models.py

from django.db import models
from django.contrib.auth.models import User
import uuid



class Entry(models.Model):
    """Model representing template content."""
    title = models.CharField(max_length=200)
    content = models.TextField('Content', help_text='Enter the Markdown content for the page.')
    creator = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

