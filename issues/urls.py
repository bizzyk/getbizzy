# issues/urls.py
from django.urls import path, include
from . import views
from rest_framework.routers import DefaultRouter
from issues.views import EntryViewSet, CreateIssueView
from rest_framework import routers

# Initialize the router
router = DefaultRouter()
router.register(r'entries', EntryViewSet)
router.register('entries/mine', EntryViewSet, basename='user-entries')

# List the router and your custom urlpatterns
urlpatterns = [
    path('', include(router.urls)),
    path('create-issue/', CreateIssueView.as_view(), name='create-issue'),
    path('home/', views.HomeView.as_view(), name ='home'),
    path('logout/', views.LogoutView.as_view(), name ='logout'),
    path('api/assistant/', views.assistant_response, name='assistant-response'),
 ]