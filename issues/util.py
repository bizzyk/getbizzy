# util.py

import jwt
import re
from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage
from django.http import JsonResponse
from functools import wraps


def validate_supabase_token(token):
    """Validate the JWT token from Supabase."""
    secret_key = settings.SUPABASE_JWT_SECRET
    issuer_url = 'https://sbvgwcfrgmazinnvqeyw.supabase.co/auth/v1'

    try:
        decoded = jwt.decode(token, secret_key, algorithms=["HS256"], audience='authenticated', issuer=issuer_url)
        return decoded
    except jwt.ExpiredSignatureError:
        return 'Token expired'
    except jwt.InvalidTokenError:
        return 'Invalid token'
    except Exception as e:
        # Catch-all for any other jwt.decode() exceptions
        return str(e)


def supabase_auth_required(view_func):
    """Decorator to check for valid Supabase JWT on requests."""
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        # Token is expected to be in the 'Authorization' header as 'Bearer <token>'
        token = request.headers.get('Authorization', '').split(' ')[1]
        validation_result = validate_supabase_token(token)

        # If validation_result is a string, an error occurred
        if isinstance(validation_result, str):
            return JsonResponse({'error': validation_result}, status=401)

        # Token is valid; proceed with view function
        request.user_id = validation_result.get('sub')  # The subject from JWT, commonly the user ID
        return view_func(request, *args, **kwargs)
    return _wrapped_view

def list_entries():
    """Returns a list of all names of markdown entry filenames without the `.md` extension."""
    _, filenames = default_storage.listdir("entries")
    return sorted(filename.rstrip(".md") for filename in filenames if filename.endswith(".md"))

def save_entry(title, content):
    """Saves or updates a markdown entry by title."""
    filename = f"entries/{title}.md"
    default_storage.save(filename, ContentFile(content))

def get_entry(title):
    """Retrieves content of a markdown entry by title."""
    filename = f"entries/{title}.md"
    try:
        with default_storage.open(filename) as f:
            return f.read().decode("utf-8")
    except FileNotFoundError:
        return None

def delete_entry(title):
    """Deletes a markdown entry by its title."""
    filename = f"entries/{title}.md"
    default_storage.delete(filename)

#
# def list_entries():
#     """
#     Returns a list of all names of template_files entries.
#     """
#     _, filenames = default_storage.listdir("entries")
#     return list(sorted(re.sub(r"\.md$", "", filename)
#                 for filename in filenames if filename.endswith(".md")))
#
#
# def save_entry(title, content):
#     """
#     Saves a template_files entry, given its title and Markdown
#     content. If an existing entry with the same title already exists,
#     it is replaced.
#     """
#     filename = f"entries/{title}.md"
#     if default_storage.exists(filename):
#         default_storage.delete(filename)
#     default_storage.save(filename, ContentFile(content))
#
#
# def get_entry(title):
#     """
#     Retrieves a template_files entry by its title. If no such
#     entry exists, the function returns None.
#     """
#     try:
#         f = default_storage.open(f"entries/{title}.md")
#         return f.read().decode("utf-8")
#     except FileNotFoundError:
#         return None
#
#
# def delete_entry(title):
#     """
#     Deletes a template entry by its title.
#     """
#     filename = f"entries/{title}.md"
#     if default_storage.exists(filename):
#         default_storage.delete(filename)
