# signals.py
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth import get_user_model
from .models import Entry

User = get_user_model()

@receiver(post_save, sender=User)
def create_default_entries(sender, instance, created, **kwargs):
    if created:  # Check if it's a new user
        Entry.objects.create(title='Welcome!', content='This is your first entry.', creator=instance)
