# issues/management/commands/populate_supabase_users.py
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from issues.models import SupabaseUser

class Command(BaseCommand):
    help = 'Populate the SupabaseUser table from the auth.users table'

    def handle(self, *args, **kwargs):
        for user in User.objects.all():
            # Use the email and name fields from the auth user
            supabase_user, created = SupabaseUser.objects.update_or_create(
                user=user,
                defaults={
                    'email': user.email,
                    'name': user.get_full_name(),
                }
            )

            if created:
                self.stdout.write(f"SupabaseUser created for {user.username}")
            else:
                self.stdout.write(f"SupabaseUser updated for {user.username}")
