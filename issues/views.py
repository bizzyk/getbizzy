
# issues/views.py
from rest_framework.decorators import api_view
import openai
import os
from dotenv import load_dotenv
from django.urls import reverse, reverse_lazy
from django.views import generic
from rest_framework import serializers, status, viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Entry
from .serializers import EntrySerializer, CreateIssueSerializer
from rest_framework import permissions
import requests

# Load environment variables
load_dotenv()

# Initialize OpenAI
openai.api_key = os.getenv('OPENAI_API_KEY')
model = os.getenv('OPENAI_API_MODEL')

@api_view(['POST'])
def assistant_response(request):
    try:
        prompt = request.data.get('prompt', '')
        if not prompt:
            return Response({'error': 'No prompt provided'}, status=400)

        response = openai.Completion.create(
            model=model,
            prompt=prompt,
            max_tokens=100
        )

        return Response({'result': response.choices[0].text.strip()})
    except Exception as e:
        return Response({'error': str(e)}, status=500)

class IsCreatorOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow the creator of an entry to edit or delete it.
    Other authenticated users can only view the entries.
    """
    def has_object_permission(self, request, view, obj):
        # SAFE_METHODS are read-only methods (GET, HEAD, OPTIONS)
        if request.method in permissions.SAFE_METHODS:
            return True
        # Only the creator of the entry can modify it
        return obj.creator == request.user

class EntryViewSet(viewsets.ModelViewSet):
    permission_classes = [IsCreatorOrReadOnly, IsAuthenticated]
    serializer_class = EntrySerializer
    queryset = Entry.objects.all()

    def get_queryset(self):
        # Filter to only show entries belonging to the authenticated user
        return Entry.objects.filter(creator=self.request.user)

    def perform_create(self, serializer):
        # Automatically assign the authenticated user as the creator
        serializer.save(creator=self.request.user)

# Other Views and Utility APIs
class CreateIssueView(APIView):
    def post(self, request, *args, **kwargs):
        serializer = CreateIssueSerializer(data=request.data)
        if serializer.is_valid():
            data = serializer.validated_data
            repo_path = data['repo_url'].split('gitlab.com/')[-1]

            headers = {
                'Private-Token': data['access_token']
            }
            issue_data = {
                'title': data['title'],
                'description': data['description'],
                'labels': ','.join(data['labels'])
            }

            response = requests.post(
                f'https://gitlab.com/api/v4/projects/{repo_path}/issues',
                headers=headers,
                data=issue_data
            )

            if response.status_code == 201:
                return Response({'message': 'Issue created successfully!'}, status=status.HTTP_201_CREATED)
            else:
                return Response({'message': 'Failed to create issue on GitLab'}, status=response.status_code)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class HomeView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        content = {'message': 'Welcome to the Authentication page using React Js and Django!'}
        return Response(content)

class LogoutView(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()
            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)

class EntryListView(generic.ListView):
    model = Entry
    permission_classes = [IsAuthenticated]


class EntryCreate(generic.CreateView):
    model = Entry
    fields = '__all__'
    initial = {'Title': 'Enter the Markdown content for the page.'}

class EntryUpdate(generic.UpdateView):
    model = Entry
    fields = ['title', 'content']

class EntryDelete(generic.DeleteView):
    model = Entry
    success_url = reverse_lazy('index')

