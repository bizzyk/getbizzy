# issues/serializers.py

from rest_framework import serializers
from .models import Entry

class EntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Entry
        fields = ['id', 'title', 'content', 'creator']
        read_only_fields = ['id', 'creator']  # Make creator read-only

class CreateIssueSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=255)
    description = serializers.CharField()
    labels = serializers.ListField(child=serializers.CharField(max_length=255))
    repo_url = serializers.URLField()
    access_token = serializers.CharField(max_length=255)