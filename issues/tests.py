from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

class AuthenticationTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_invalid_jwt_token(self):
        self.client.credentials(HTTP_AUTHORIZATION='Bearer invalidtoken')
        response = self.client.get('/api/secure-endpoint')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
