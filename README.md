# getBizzy.pro

https://youtu.be/FK33HD3nKfw

## AI-Powered Product Management

## Description
This project is an artificial intelligence-powered web application designed to facilitate product management through automated issue creation. Built using a Django API and ReactJS frontend, it integrates with Supabase for database and storage solutions. The application supports OAuth for authentication with Google, GitLab, and GitHub, enabling users to manage and automate their product management tasks efficiently.

## Files and Directory Structure

![folder.png](staticfiles/folder.png)

## Installation

1. Clone the repository:
   git clone https://gitlab.com/bizzyk/getbizzy.git
2. Navigate to the project directory:
   cd getBizzy
3. Install backend dependencies:
    pip install -r requirements.txt
4. Install frontend dependencies:
    npm install


## Running the Application

1. Start the backend server:
    python manage.py runserver
2. Start the frontend application:
    npm start

### Usage

- **Login**: Users can log in using their Google, GitLab, or GitHub accounts.
- **Settings Page**: Users can enter the necessary information for each repository they connect to, including username, repository URL, and access token.
- **Issue Creation**: Users can create issues through a chat interface. They select the repository, issue type, and labels via dropdown menus. After describing the issue, the application retrieves a markdown template that users can edit and finalize before submitting to create the issue in GitLab.

### OpenAI Integration

- This application utilizes the OpenAI API to enhance the user interface by dynamically generating content based on user inputs. This integration allows for sophisticated data processing and automated issue template generation, making the issue creation process more efficient and user-friendly.

### Design Decisions

- **ReactJS and Django**: Chosen for their robust ecosystem and support for rapid development of interactive web applications.
- **Supabase**: Provides scalable backend services, including database management, authentication, and storage.
- **OAuth Integration**: Facilitates secure and versatile user authentication.
- **AI-Powered Interface**: Enhances user interaction by generating dynamic content based on user inputs.

### Future Enhancements

- Addition of sprint report generation.
- Integration of interactive demo deck creation via AI.

### Additional Notes

- Ensure all environment variables are set according to the `.env.example` file before starting the application.
- If you encounter any issues, contact the developer.

### Contact Information

- **Developer**: Elizabeth Koch
- **Email**: [info@getBizzy.pro](mailto:info@getBizzy.pro)
- **GitHub**: [https://github.com/bizzyK](https://github.com/bizzyK)
- **GitLab**: [https://gitlab.com/bizzyK](https://gitlab.com/bizzyK)
- **LinkedIn**: [https://www.linkedin.com/in/elizabethkoch/](https://www.linkedin.com/in/elizabethkoch/)

![1.png](staticfiles/1.png)
![2.png](staticfiles/2.png)
![3.png](staticfiles/3.png)