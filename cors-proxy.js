// Path: cors-proxy.js

const cors_proxy = require('cors-anywhere');

const host = 'localhost';
const port = 8080; // Change the port number

cors_proxy.createServer({
    originWhitelist: [], // Allow all origins
    requireHeader: ['origin', 'x-requested-with'],
    removeHeaders: ['cookie', 'cookie2']
}).listen(port, host, () => {
    console.log(`CORS Anywhere proxy running on ${host}:${port}`);
});
