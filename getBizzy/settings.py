# settings.py

from datetime import timedelta
import environ
import os
from pathlib import Path

# Initialize environ
env = environ.Env()
# Set the correct path to the .env file
BASE_DIR = Path(__file__).resolve().parent.parent
env_file = os.path.join(BASE_DIR, '.env')
# Read the .env file
environ.Env.read_env(env_file)

# SECURITY SETTINGS
SECRET_KEY = env('DJANGO_SECRET_KEY')
DEBUG = env.bool('DEBUG', default=False)

ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default=['127.0.0.1', 'localhost', 'localhost:3000', 'localhost:8000', 'getbizzy.pro'])

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        '': {  # root logger
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

# DATABASE CONFIGURATION
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env('DB_NAME'),
        'USER': env('DB_USER'),
        'PASSWORD': env('DB_PASSWORD'),
        'HOST': env('DB_HOST'),
        'PORT': env('DB_PORT', default='5432'),
    }
}

# APPLICATION DEFINITION
INSTALLED_APPS = [
    'corsheaders',
    'issues',
    'rest_framework',
    'csp',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'csp.middleware.CSPMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'getBizzy.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'getBizzy.wsgi.application'

# INTERNATIONALIZATION AND LOCALIZATION
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_TZ = True

# STATIC FILES CONFIGURATION
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'build', 'static')]
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

# EMAIL CONFIGURATION
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp-relay.gmail.com'
EMAIL_PORT = 465
EMAIL_USE_TLS = False
EMAIL_USE_SSL = True
EMAIL_HOST_USER = env('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')

# REST FRAMEWORK AND JWT CONFIGURATION
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [],
    'DEFAULT_PERMISSION_CLASSES': [],
}


SIMPLE_JWT = {
    'ALGORITHM': 'HS256',
    'SIGNING_KEY': env('SUPABASE_JWT_SECRET'),
    'VERIFYING_KEY': None,  # Adjust as needed for RS256
    'AUDIENCE': 'authenticated',
    'ISSUER': env('SUPABASE_URL'),
    'AUTH_HEADER_TYPES': ('Bearer',),
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=30),
}

# SUPABASE CONFIGURATION
SUPABASE_CLIENT_API_KEY = env('SUPABASE_CLIENT_API_KEY')
SUPABASE_SERVICE_KEY = env('SUPABASE_SERVICE_KEY')
SUPABASE_URL = env('SUPABASE_URL')
SUPABASE_JWT_SECRET = os.getenv('SUPABASE_JWT_SECRET')

# CSP CONFIGURATION
CSP_FRAME_ANCESTORS = ("'self'", "http://localhost:3000")
CSP_DEFAULT_SRC = ("'self'",)
CSP_STYLE_SRC = ("'self'", "https://stackpath.bootstrapcdn.com", "https://fonts.googleapis.com", "'unsafe-inline'")
CSP_SCRIPT_SRC = ("'self'", "https://code.jquery.com", "https://cdn.jsdelivr.net", "https://stackpath.bootstrapcdn.com", "'unsafe-inline'", "'unsafe-eval'")
CSP_FONT_SRC = ("'self'", "https://fonts.gstatic.com")
CSP_IMG_SRC = ("'self'", "https://repo.bespin.cce.af.mil", "data:")

# CORS CONFIGURATION
CORS_ALLOWED_ORIGINS = [
    "http://localhost:3000",
]
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_ALL_ORIGINS = True  # Consider setting this to False in production
CORS_ALLOWED_ORIGINS = ["http://localhost:3000"]  # Add other origins as needed
CORS_ORIGIN_WHITELIST = [
    "http://localhost:3000",
    "http://127.0.0.1:8000",
    "http://127.0.0.1:3000",
    "http://localhost:8000",
    "https://getbizzy.pro",
    "https://getbizzy.github.io/getBizzy",
]
# EXTERNAL SERVICE API KEYS
GOOGLE_CLIENT_ID = env('GOOGLE_CLIENT_ID')
GOOGLE_SECRET = env('GOOGLE_SECRET')
GITHUB_CLIENT_ID = env('GITHUB_CLIENT_ID')
GITHUB_SECRET = env('GITHUB_SECRET')
GITLAB_CLIENT_ID = env('GITLAB_CLIENT_ID')
GITLAB_SECRET = env('GITLAB_SECRET')
OPENAI_API_KEY = env('OPENAI_API_KEY')
OPENAI_API_MODEL = env('OPENAI_API_MODEL')

# Default primary key field type
DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
#
# # Environment Variables
# SECRET_KEY = env('DJANGO_SECRET_KEY')
# DEBUG = env.bool('DEBUG', default=False)
# ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default=['127.0.0.1', 'localhost'])
# DJANGO_SECRET_KEY = env('DJANGO_SECRET_KEY')
#
# # Database Configuration
# DB_NAME = env('DB_NAME')
# DB_USER = env('DB_USER')
# DB_PASSWORD = env('DB_PASSWORD')
# DB_HOST = env('DB_HOST')
# DB_PORT = env('DB_PORT', default='5432')
#
# # Email Configuration
# EMAIL_HOST_USER = env('EMAIL_HOST_USER')
# EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')
#
# # Supabase Configuration
# SUPABASE_CLIENT_API_KEY = env('SUPABASE_CLIENT_API_KEY')
# SUPABASE_SERVICE_KEY = env('SUPABASE_SERVICE_KEY')
# SUPABASE_URL = env('SUPABASE_URL')
# SUPABASE_JWT_SECRET = env('SUPABASE_JWT_SECRET')
#
# # External Service API Keys
# GOOGLE_CLIENT_ID = env('GOOGLE_CLIENT_ID')
# GOOGLE_SECRET = env('GOOGLE_SECRET')
# GITHUB_CLIENT_ID = env('GITHUB_CLIENT_ID')
# GITHUB_SECRET = env('GITHUB_SECRET')
# GITLAB_CLIENT_ID = env('GITLAB_CLIENT_ID')
# GITLAB_SECRET = env('GITLAB_SECRET')
# OPENAI_API_KEY = env('OPENAI_API_KEY')
# OPENAI_API_MODEL = env('OPENAI_API_MODEL')
#
# # Initialize environ
# env = environ.Env()
# # Set the correct path to the .env file
# BASE_DIR = Path(__file__).resolve().parent.parent
# env_file = os.path.join(BASE_DIR, '.env')
# # Read the .env file
# environ.Env.read_env(env_file)
#
# # SIMPLE_JWT = {
# #     'ALGORITHM': 'RS256',
# #     'SIGNING_KEY': None,  # You don't need to set this for RS256
# #     'VERIFYING_KEY': 'PUBLIC_SUPABASE_ANON_KEY',  # Public key provided by Supabase
# #     'AUDIENCE': None,
# #     'ISSUER': 'PUBLIC_SUPABASE_URL',
# #     'AUTH_HEADER_TYPES': ('Bearer',),
# #     'ACCESS_TOKEN_LIFETIME': timedelta(minutes=30),
# # }
#
# JWT_SECRET = os.environ.get('SUPABASE_JWT_SECRET')
#
# SIMPLE_JWT = {
#     'ALGORITHM': 'HS256',  # Example algorithm
#     'SIGNING_KEY': os.getenv('SUPABASE_JWT_SECRET'),
#     'VERIFYING_KEY': os.getenv('PUBLIC_SUPABASE_ANON_KEY'),
#     'AUDIENCE': 'authenticated',  # Adjust based on your expected audience
#     'ISSUER': os.getenv('PUBLIC_SUPABASE_URL'),
#     'AUTH_HEADER_TYPES': ('Bearer',),
#     'ACCESS_TOKEN_LIFETIME': timedelta(minutes=30),
# }
#
#
#
#
# CSP_FRAME_ANCESTORS = ("'self'", "http://localhost:3000")
#
# CORS_ALLOW_ALL_ORIGINS = True  # For development only, restrict this in production
#
#
# CORS_ALLOWED_ORIGINS = [
#     "http://localhost:3000",  # React development server
# ]
#
#
# STATIC_URL = '/static/'
# STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
# STATICFILES_DIRS = [
#     os.path.join(BASE_DIR, 'build', 'static'),  # Assuming React build files are here
# ]
# STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
#
#
# # Quick-start development settings - unsuitable for production
# # See https://docs.djangoproject.com/en/5.0/howto/deployment/checklist/
#
# # SECURITY WARNING: keep the secret key used in production secret!
# SECRET_KEY = env('DJANGO_SECRET_KEY')
#
# # SECURITY WARNING: don't run with debug turned on in production!
# DEBUG = env.bool('DEBUG', default=False)
#
# ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default=['127.0.0.1', 'localhost', 'localhost:3000', 'localhost:8000', 'getbizzy.pro', 'getbizzy.github.io/getBizzy'])
#
# # Application definition
#
# INSTALLED_APPS = [
#     'issues',
#     'csp',
#     'corsheaders',
#     'rest_framework',
#     'django.contrib.admin',
#     'django.contrib.auth',
#     'django.contrib.contenttypes',
#     'django.contrib.sessions',
#     'django.contrib.messages',
#     'django.contrib.sites',
#     'django.contrib.staticfiles',
# ]
#
#
# REST_FRAMEWORK = {
#     'DEFAULT_AUTHENTICATION_CLASSES': (
#         'rest_framework_simplejwt.authentication.JWTAuthentication',
#     ),
# }
#
#
# MIDDLEWARE = [
#     'corsheaders.middleware.CorsMiddleware',
#     'csp.middleware.CSPMiddleware',
#     'django.middleware.security.SecurityMiddleware',
#     'whitenoise.middleware.WhiteNoiseMiddleware',
#     'django.contrib.sessions.middleware.SessionMiddleware',
#     'django.middleware.common.CommonMiddleware',
#     'django.middleware.csrf.CsrfViewMiddleware',
#     'django.contrib.auth.middleware.AuthenticationMiddleware',
#     'django.contrib.messages.middleware.MessageMiddleware',
#     'django.middleware.clickjacking.XFrameOptionsMiddleware',
# ]
#
# ROOT_URLCONF = 'getBizzy.urls'
#
# TEMPLATES = [
#     {
#         'BACKEND': 'django.template.backends.django.DjangoTemplates',
#         'DIRS': [],
#         'APP_DIRS': True,
#         'OPTIONS': {
#             'context_processors': [
#                 'django.template.context_processors.debug',
#                 'django.template.context_processors.request',
#                 'django.contrib.auth.context_processors.auth',
#                 'django.contrib.messages.context_processors.messages',
#             ],
#         },
#     },
# ]
#
#
# WSGI_APPLICATION = 'getBizzy.wsgi.application'
#
# CSP_DEFAULT_SRC = ("'self'", )
# CSP_STYLE_SRC = ("'self'", "https://stackpath.bootstrapcdn.com", "https://fonts.googleapis.com", "'unsafe-inline'")
# CSP_SCRIPT_SRC = ("'self'", "https://code.jquery.com", "https://cdn.jsdelivr.net", "https://stackpath.bootstrapcdn.com", "'unsafe-inline'", "'unsafe-eval'")
# CSP_FONT_SRC = ("'self'", "https://fonts.gstatic.com")
# CSP_IMG_SRC = ("'self'", "https://repo.bespin.cce.af.mil", "data:")
#
#
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql',
#         'NAME': env('DB_NAME'),
#         'USER': env('DB_USER'),
#         'PASSWORD': env('DB_PASSWORD'),
#         'HOST': env('DB_HOST'),
#         'PORT': env('DB_PORT', default='5432'),
#     }
# }
#
#
# # Password validation
# # https://docs.djangoproject.com/en/5.0/ref/settings/#auth-password-validators
#
# AUTH_PASSWORD_VALIDATORS = [
#     {
#         'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
#     },
# ]
#
#
# # Internationalization
# # https://docs.djangoproject.com/en/5.0/topics/i18n/
#
# LANGUAGE_CODE = 'en-us'
#
# TIME_ZONE = 'UTC'
#
# USE_I18N = True
#
# USE_TZ = True
#
#
#
# # Default primary key field type
# # https://docs.djangoproject.com/en/5.0/ref/settings/#default-auto-field
#
# DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'
#
# # Additional settings for django-allauth
# AUTHENTICATION_BACKENDS = (
#     'django.contrib.auth.backends.ModelBackend',
#     'allauth.account.auth_backends.AuthenticationBackend',
# )
#
# SITE_ID = 1  # Required for django-allauth
#
# ACCOUNT_EMAIL_REQUIRED = True
# ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
# LOGIN_REDIRECT_URL = '/'  # Adjust based on your needs
#
# # Django test email backend
# # EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
#
# # Django prod email backend
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_HOST = 'smtp-relay.gmail.com'
# EMAIL_PORT = 465
# EMAIL_USE_TLS = False
# EMAIL_USE_SSL = True
# EMAIL_HOST_USER = env('EMAIL_HOST_USER')
# EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')
#
