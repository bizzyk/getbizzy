# middleware.py

from django.http import JsonResponse
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework import exceptions

class JWTAuthenticationMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        try:
            user_auth_tuple = JWTAuthentication().authenticate(request)
            if user_auth_tuple is not None:
                request.user, request.token = user_auth_tuple
        except exceptions.AuthenticationFailed:
            return JsonResponse({'detail': 'Authentication credentials were not provided or were invalid.'}, status=401)

        response = self.get_response(request)
        return response
