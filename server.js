const express = require('express');
const path = require('path');
const serveStatic = require('serve-static');

const app = express();

// Middleware to set the Permissions-Policy header
app.use((req, res, next) => {
    res.setHeader('Permissions-Policy', 'clipboard-write=(self "http://localhost:3000")');
    next();
});

const cors = require('cors');

// Middleware to enable CORS
app.use(cors());

// Serve static files from the build directory
app.use(serveStatic(path.join(__dirname, 'build')));

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
